"use strict";

var express = require('express');
var app = express();
var swig = require("swig");
var path = require('path');


swig = new swig.Swig();
app.engine('html', swig.renderFile );
app.set("view engine","html");

app.set("views", path.join(__dirname,"/templates"));

var routes = require("./routes");

app.use('/static', express.static('./static'));

app.use( routes );



var port = process.env.PORT || 81;
app.listen( port, function(){
    console.log('*** process started on port: ', port );
});


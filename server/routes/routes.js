"use strict";
var router = require('express').Router()
var c = require('../controllers');



router.use('^/teamlist', c.root.teamlist );
router.use('^/zones', c.root.zones );
router.use('^/topphotos/', c.root.topphotos );
router.use('^/$', c.root.teams );

router.use( function(req, res){
	res.send(404, 'not found');
});

module.exports = router;

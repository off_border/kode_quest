"use strict";

var fs = require("fs");
var path = require("path");

var files = fs.readdirSync( __dirname );

console.log('--- files:', files);

var files = fs.readdirSync( __dirname ).filter( f  => path.extname(f) === ".js" && f !== path.basename(__filename) );


files.forEach( (f) => {
   let module_name = f.replace('.js','');
   module.exports[module_name] = require( __dirname + "/" + f );
});


console.log('--- modules for ' + path.basename(__filename) + ':', files );


var request = require('request');
var cheerio = require('cheerio');

var base_addr = 'http://138.68.84.25:8000/';

module.exports.teams = function(req, res ){

	return request( base_addr, (err, resp, body) => {
		if( err )
			return res.render('cannot get response:', err);
		var $ = cheerio.load(body);
		var rows = $('.caption');
		var data = [];
		rows.each( function() {
			if( data.length > 100 )
				return;
			var avatar = $('img', this).eq(0).attr('src');
			var image;
			if( avatar )
				image = { url : base_addr + avatar };
			else
				image = false;

			var top_photos = mapRows($(this), 'hr ~ div',
				($elem)=>{
						return{
							scores: $elem.find('strong').text(),
							image: { url : base_addr+$elem.find('img').attr('src') }
						};
					});


			if( top_photos.length > 0 )
				top_photos = { all: top_photos };
			else
				top_photos = false;

			data.push({
					id: $('h4 strong', this).eq(0).text().split(' - ')[1]
					,num: $('h4 strong', this).eq(0).text().split(' - ')[0]
					// ,not_reg: $('.btn-danger', this).text()
					,zone: $('h4 strong', this).eq(1).text()
					,scores: $('p strong', this).eq(0).text()
					,image_scores: $('p strong', this).eq(1).text()
					,time_minutes: $('p strong', this).eq(2).text()
					,quest_completed: $('p strong', this).eq(3).text() == 'ДА' ? 'yes' : 'no'
					,members: $('p strong', this).eq(4).text().replace(/\s+/gi, ' ').split(/,\s*/gi)
					,image: image
					,top_photos: top_photos
			});
		});

		console.log('--- data: \n', data);
		res.locals.teams = data;
		res.locals.page = 'main';
		res.render("app/teams");
	});
};

module.exports.teamlist = function(req, res){
	return mapRows(base_addr+'teamlist/', '.col-md-12 h4',
	($elem) =>{
		return {
			id: $elem.text().split(' - ')[1],
			num: $elem.text().split(' - ')[0]
		}
	},
	(err, data) => {
		console.log('--- data: \n', data);
		res.locals.teams = data;
		res.locals.page = 'teamlist'
		res.render("app/teamlist");
	 });

};

module.exports.zones = function(req,res){
	return mapRows(base_addr+'zones/', '.row-fluid .col-md-12',
	($elem,$) =>{
		var waypoints = mapRows($elem, 'strong + img', ($img)=>{
			return{
				title: $img.prev().text(),
				qr_code: { url: base_addr+$img.attr('src') }
			}
		});
		return {
			id: $elem.find('h4 strong').text(),
			waypoints: waypoints
		}
	},
	(err, data) => {
		console.log('--- data: \n', data);
		res.locals.zones = data;
		res.locals.page = 'zones'
		res.render("app/zones");
	 });
}


module.exports.topphotos = function(req,res){
	return mapRows(base_addr+'topphotos/', 'img + form',
	($elem) =>{
		return {
			image: {url: base_addr+$elem.prev().attr('src').replace(/^\//,'') },
			id: $elem.attr('action').split('/')[2]
		}
	},
	(err, data) => {
		console.log('--- data: \n', data);
		res.locals.photos = data;
		res.locals.page = 'topphotos'
		res.render("app/topphotos");
	 });
}



function mapRows(url, selector, mapFn, callback, limit) {
	if( typeof url === 'string'){
		request(url, (err, resp, body) =>{
			if( err ){
				console.log('ERROR: cannot load url:', url)
				callback( err );
			}
			var $ = cheerio.load(body);
			var rows = $( selector );
			callback( err, map(rows) );
		});
	} else {
		var rows = url.find(selector);
		return map(rows);
	}

	function map(rows){
		var data = []
		for( var i = 0; i < rows.length; i++ ){
			if( limit && data.length >= limit )
				break;
			data.push( mapFn( rows.eq(i) ) );
		}
		return data;
	}

}
